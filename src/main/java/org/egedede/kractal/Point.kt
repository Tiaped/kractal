package org.egedede.kractal

import kotlin.math.pow

data class Point(val x: Double, val y: Double) {
    operator fun plus(otherPoint: Point): Point {
        return Point(x + otherPoint.x, y + otherPoint.y)
    }

    operator fun minus(otherPoint: Point): Point {
        return Point(x - otherPoint.x, y - otherPoint.y)
    }

    operator fun times(otherPoint: Point): Point {
        return Point((x * otherPoint.x - y * otherPoint.y), x * otherPoint.y + y * otherPoint.x)
    }

    fun abs(): Double {
        return (x.pow(2) + y.pow(2)).pow(0.5)
    }

}
