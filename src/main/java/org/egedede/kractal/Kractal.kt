package org.egedede.kractal

import java.awt.Color
import java.awt.Desktop
import java.awt.image.BufferedImage
import java.nio.file.Files
import javax.imageio.ImageIO


/**
 * Args
 * <ul>
 *     <ol>bottom left x</ol>
 *     <ol>bottom left y</ol>
 *     <ol>top right x</ol>
 *     <ol>top right y</ol>
 *     <ol>display size : a square</ol>
 *     <ol>nb iteration</ol>
 *     <ol></ol>
 */
fun main(args: Array<String>) {
    val bottomLeft = Point(args[0].toDouble(), args[1].toDouble())
    val topRightHandler = Point(args[2].toDouble(), args[3].toDouble())
    val vector = topRightHandler - bottomLeft
    val width = vector.x
    val height = vector.y
    val step = args[4].toDouble()

    val dataWidth = width / step + 1
    val dataHeight = height / step + 1
    val data = Array(dataWidth.toInt()) { Array(dataHeight.toInt()) { 0 } }
    val maxIteration = args[5].toInt()
    val points = mutableListOf<KractalPoint>()
    var xCoordinates = bottomLeft.x
    var i = 0
    while (xCoordinates < topRightHandler.x) {
        var yCoordinates = bottomLeft.y
        var j = 0
        xCoordinates += step
        while (yCoordinates < topRightHandler.y) {
            yCoordinates += step
            points.add(KractalPoint(Point(xCoordinates, yCoordinates), i,j))
            j++
        }
        i++
    }
    points.forEach { data[it.x][it.y] = compute(it.point, maxIteration) }
    store(data)
}

private fun compute(point: Point, maxIteration: Int): Int {
    val iteration = doCompute(point, point, maxIteration)
    return maxIteration - iteration
}

fun doCompute(computation: Point, source: Point, remainingIterations: Int): Int {
    val point = computation * computation + source
    if (point.abs() >= 2) return remainingIterations
    if (remainingIterations == 1) return 0
    return doCompute(point, source, remainingIterations - 1)

}

private fun store(data: Array<Array<Int>>) {
    val result = BufferedImage(data.size, data[0].size, BufferedImage.TYPE_3BYTE_BGR)
    for (i in data.indices) {
        for (j in 0 until data[0].size) {
            val value = data[i][j]
            result.setRGB(i, j, Color(255 - value, 255 - value, 255 - value).rgb)
        }
    }
    val file = Files.createTempFile("prefix", "suffix")
    println(file)
    ImageIO.write(result, "PNG", file.toFile())
    Desktop.getDesktop().open(file.toFile())
}
