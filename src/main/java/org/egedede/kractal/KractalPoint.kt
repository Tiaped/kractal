package org.egedede.kractal

data class KractalPoint(val point: Point, val x: Int, val y: Int)
