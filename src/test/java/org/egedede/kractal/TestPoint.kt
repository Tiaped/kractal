package org.egedede.kractal

import org.junit.Test
import kotlin.math.pow

class TestPoint {

    @Test
    fun testPoint() {
        val point = Point(1.0,1.0)
        println(2.0.pow(0.5))
        printPoint(point)

        printPoint(Point(0.0, 1.0))
        printPoint(Point(1.0, 0.0))
    }

    private fun printPoint(point: Point) {
        println(point)
        println(point.abs())
        val pow2 = point * point
        println(pow2)
        println(pow2.abs())
    }
}